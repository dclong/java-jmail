package dclong.net;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.Address;
import javax.mail.BodyPart;
import javax.mail.Message.RecipientType;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Part;
import javax.mail.Session;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

public abstract class JavaMailer {
	private class Attachment{
		protected String orginalName;
		protected File file;
		protected InputStream is;
		public Attachment(String originalName, File file, InputStream is){
			this.orginalName = originalName;
			this.file = file;
			this.is = is;
		}
	}
	protected Properties props;
	protected Session session;
	protected MimeMessage msg;
	protected ArrayList<Attachment> atts;
	/**
	 * Get all the recipient addresses for the message. 
	 * The default implementation extracts the TO, CC, and BCC recipients using the getRecipients method.
	 * 
	 * This method returns null if none of the recipient headers are present in this message. 
	 * It may Return an empty array if any recipient header is present, 
	 * but contains no addresses.
	 * @return array of Address objects.
	 */
	public Address[] getAllRecipients() {
		try {
			return msg.getAllRecipients();
		} catch (MessagingException e) {
			e.printStackTrace();
			System.out.println("Unable to get all recipients.");
			return null;
		}
	}
	/**
	 * Returns the "From" attribute. 
	 * The "From" attribute contains the identity of the person(s) who wished this message to be sent.
	 * In certain implementations, 
	 * this may be different from the entity that actually sent the message.
	 * 
	 * This method returns null if this attribute is not present in this message. Returns an empty array if this attribute is present, 
	 * but contains no addresses.
	 * @return an array of email addresses.
	 */
	public Address[] getFrom() {
		try {
			return msg.getFrom();
		} catch (MessagingException e) {
			e.printStackTrace();
			System.out.println("Unable to get from addresses.");
			return null;
		}
	}
	/**
	 * Get the date this message was received.
	 * @return the date this message was received.
	 */
	public Date getReceivedDate() {
		try {
			return msg.getReceivedDate();
		} catch (MessagingException e) {
			e.printStackTrace();
			System.out.println("Unable to get the received date.");
			return null;
		}
	}
	/**
	 * Get the date this message was sent.
	 * @return the date this message was sent.
	 */
	public Date getSentDate() {
		try {
			return msg.getSentDate();
		} catch (MessagingException e) {
			e.printStackTrace();
			System.out.println("Unable to get the received date.");
			return null;
		}
	}
	/**
	 * Get recipient addresses of the type "To".
	 * 
	 * This method returns null if no recipients of type "To" 
	 * are present in this message. 
	 * It may return an empty array if the header is present, but contains no addresses.
	 * @return an array of email addresses.
	 */
	public Address[] getTo() {
		try {
			return msg.getRecipients(RecipientType.TO);
		} catch (MessagingException e) {
			e.printStackTrace();
			System.out.println("Unable to get \"TO\" addresses");
			return null;
		}
	}
	/**
	 * Returns the value of the RFC 822 "Sender" header field. If the "Sender" header field is absent, null is returned.
	 * 
	 * This implementation uses the getHeader method to obtain the requisite header field.
	 * @return an Address object.
	 */
	public Address getSender() {
		try {
			return  msg.getSender();
		} catch (MessagingException e) {
			e.printStackTrace();
			System.out.println("Unable to get \"TO\" addresses");
			return null;
		}
	}
	/**
	 * Return the size of the content of this message in bytes. 
	 * Return -1 if the size cannot be determined.
	 * 
	 * Note that this number may not be an exact measure of the 
	 * content size and may or may not account for any transfer encoding of the content.
	 * @return size of content in bytes.
	 */
	public int getSize() {
		try {
			return msg.getSize();
		} catch (MessagingException e) {
			e.printStackTrace();
			System.out.println("Unable to get the size of the message");
			return -1;
		}
	}
	/**
	 * Updates the appropriate header fields of this message 
	 * to be consistent with the message's contents. 
	 * 
	 * If this message is contained in a Folder, 
	 * any changes made to this message are committed to the containing folder.
	 * 
	 * If any part of a message's headers or contents are changed, 
	 * saveChanges must be called to ensure that those changes are permanent. 
	 * Otherwise, any such modifications may or may not be saved, 
	 * depending on the folder implementation.
	 * 
	 * Messages obtained from folders opened READ_ONLY should not be modified and saveChanges should not be called on such messages.
	 * @return a boolean value indicating whether the 
	 * operation was successful.
	 */
	public boolean saveChanges() {
		try {
			msg.saveChanges();
			return true;
		} catch (MessagingException e) {
			e.printStackTrace();
			System.out.println("Unable to save changes to the message.");
			return false;
		}
	}

	public Address[] getReplyTo() {
		try {
			return msg.getReplyTo();
		} catch (MessagingException e) {
			e.printStackTrace();
			System.out.println("Unable to get reply-to addresses");
			return null;
		}
	}

	public Address[] getBcc() {
		try {
			return msg.getRecipients(RecipientType.BCC);
		} catch (MessagingException e) {
			e.printStackTrace();
			System.out.println("Unable to get \"BCC\" addresses");
			return null;
		}
	}

	public Address[] getCc() {
		try {
			return msg.getRecipients(RecipientType.CC);
		} catch (MessagingException e) {
			e.printStackTrace();
			System.out.println("Unable to get \"Cc\" addresses");
			return null;
		}
	}

	public String getFileName() {
		try {
			return msg.getFileName();
		} catch (MessagingException e) {
			e.printStackTrace();
			System.out
					.println("Unable to get file name associated with the message");
			return null;
		}
	}

	public String getSubject() {
		try {
			return msg.getSubject();
		} catch (MessagingException e) {
			e.printStackTrace();
			System.out.println("Unable to get the subject of the message.");
			return null;
		}
	}

	public String getPlainBody() {
		return getPlainBody(msg);
	}

	private void getContentIOException(IOException e) {
		e.printStackTrace();
		System.out
				.println("Unable to get mime content. This is probably due to loss of connection.");
	}

	private void setContentMessagingException(MessagingException e){
		e.printStackTrace();
		System.out.println("Unable to set content of the message.");
	}
	
	private void getContentMessagingException(MessagingException e) {
		e.printStackTrace();
		System.out.println("Unable to get mime content.");
	}

	private void isMimeTypeMessagingException(MessagingException e) {
		e.printStackTrace();
		System.out.println("Unable to check mime type.");
	}
	
	public String getHtmlBody(){
		return getHtmlBody(msg);
	}
	
	private void getFileNameMessagingException(MessagingException e){
		e.printStackTrace();
		System.out.println("Unable to get name of the attachment.");
	}
	
	public String[] getOriginalAttachmentNames(){
		String[] names = new String[atts.size()];
		for(int i=names.length-1; i>=0; --i){
			names[i] = atts.get(i).orginalName;
		}
		return names;
	}
	
	public String[] getLegalAttachmentNames(){
		String[] names = new String[atts.size()];
		for(int i=names.length-1; i>=0; --i){
			names[i] = atts.get(i).file.getPath();
		}
		return names;
	}
	
	public File[] downloadAttachments(){
		File[] files = new File[atts.size()];
		for(int i=files.length-1; i>=0; --i){
			Attachment att = atts.get(i);
			files[i] = att.file;
			FileOutputStream fos;
			try {
				fos = new FileOutputStream(att.file);
			} catch (FileNotFoundException e) {
				e.printStackTrace();
				System.out.println("Shall not happen");
				return null;
			}
		    BufferedOutputStream bos = new BufferedOutputStream(fos);
		    BufferedInputStream bis = new BufferedInputStream(att.is);
		    int aByte;
		    while(true){
		    	try {
					aByte = bis.read();
				} catch (IOException e) {
					e.printStackTrace();
					System.out.println("Exception occurs in reading input stream.");
					return null;
				}
		    	if(aByte!=-1){
		    		try {
						bos.write(aByte);
					} catch (IOException e) {
						e.printStackTrace();
						System.out.println("Exception occurs in writing output stream.");
						return null;
					}
		    	}else{
		    		break;
		    	}
		    	
		    }
		    try {
				bos.flush();
			} catch (IOException e) {
				e.printStackTrace();
				System.out.println("Exception occurs when flushing output stream.");
				return null;
			}
		    try {
				bos.close();
			} catch (IOException e) {
				e.printStackTrace();
				System.out.println("Exception occurs when closing output stream.");
				return null;
			}
		    try {
				bis.close();
			} catch (IOException e) {
				e.printStackTrace();
				System.out.println("Exception occurs when closing input stream");
				return null;
			}
		}
		return files;
	}
	
	public void scanAttachments(String dir){
		atts = scanAttachments(msg,dir);
	}
	
	private ArrayList<Attachment> scanAttachments(Object obj,String dir){
		String type = "TEXT/*";
		if(obj instanceof MimeMessage){
			MimeMessage mmsg = (MimeMessage) obj;
			boolean isText = false;
			try {
				isText = mmsg.isMimeType(type);
			} catch (MessagingException e) {
				isMimeTypeMessagingException(e);
				return null;
			}
			if(isText){
				return new ArrayList<Attachment>(0);
			}
			Object content = null;
			try {
				content = mmsg.getContent();
			} catch (IOException e1) {
				getContentIOException(e1);
				return null;
			} catch (MessagingException e1) {
				getContentMessagingException(e1);
				return null;
			}
			return scanAttachments(content,dir);
		}
		if (obj instanceof MimeMultipart) {
			MimeMultipart mmp = (MimeMultipart) obj;
			int count = 0;
			try {
				count = mmp.getCount();
			} catch (MessagingException e) {
				getCountMessagingException(e);
				return null;
			}
			ArrayList<Attachment> atts = new ArrayList<Attachment>(5);
			for (int i = 0; i < count; ++i) {
				MimeBodyPart mbp = null;
				try {
					mbp = (MimeBodyPart) mmp.getBodyPart(i);
				} catch (MessagingException e1) {
					getBodyPartMessagingException(e1);
					return null;
				}
				atts.addAll(scanAttachments(mbp,dir));
			}
			return atts;
		}
		if(obj instanceof MimeBodyPart){
			MimeBodyPart mbp = (MimeBodyPart) obj;
			String disPosition = null;
			try {
				disPosition = mbp.getDisposition();
			} catch (MessagingException e1) {
				getDispositionMessagingException(e1);
				return null;
			}
			ArrayList<Attachment> atts = new ArrayList<Attachment>(1);
			if (disPosition!=null && disPosition.equalsIgnoreCase(Part.ATTACHMENT)) {
					try {
						String oname = mbp.getFileName();
						File file = legalFile(oname,dir);
						InputStream is = mbp.getInputStream();
						atts.add(new Attachment(oname,file,is));
					} catch (MessagingException e) {
						getFileNameMessagingException(e);
						return null;
					} catch (IOException e) {
						getInputStreamIOException(e);
						return null;
					}
					return atts;
			}
			Object content = null;
			try {
				content = mbp.getContent();
			} catch (IOException e1) {
				getContentIOException(e1);
				return null;
			} catch (MessagingException e1) {
				getContentMessagingException(e1);
				return null;
			}
			return scanAttachments(content,dir);
		}
		return new ArrayList<Attachment>(0);
	}
	
	private void getInputStreamIOException(IOException e){
		e.printStackTrace();
		System.out.println("Unable to get object input stream of attachment.");
	}
	
	private File legalFile(String fileName, String dir){
		String fileNameWithoutExtension = fileName.substring(0,fileName.lastIndexOf("."));
		String extension = fileName.substring(fileName.lastIndexOf("."));
		File file = new File(dir,fileName);
		for (int i=1; file.exists(); i++) {
		  file = new File(dir,fileNameWithoutExtension + " (" + i + ")" + extension);
		}
		return file;
	}
	
	private String getHtmlBody(Object obj) {
		String type = "TEXT/HTML";
		if (obj instanceof MimeMessage) {
			MimeMessage mmsg = (MimeMessage) obj;
			boolean isHtml = false;
			try {
				isHtml = mmsg.isMimeType(type);
			} catch (MessagingException e) {
				isMimeTypeMessagingException(e);
				return null;
			}
			Object content = null;
			try {
				content = mmsg.getContent();
			} catch (IOException e1) {
				getContentIOException(e1);
				return null;
			} catch (MessagingException e1) {
				getContentMessagingException(e1);
				return null;
			}
			if (isHtml) {
				return (String) content;
			}
			return getHtmlBody(content);
			
		}
		if (obj instanceof MimeMultipart) {
			MimeMultipart mmp = (MimeMultipart) obj;
			int count = 0;
			try {
				count = mmp.getCount();
			} catch (MessagingException e) {
				getCountMessagingException(e);
				return null;
			}
			String s = "";
			for (int i = 0; i < count; ++i) {
				MimeBodyPart mbp = null;
				try {
					mbp = (MimeBodyPart) mmp.getBodyPart(i);
				} catch (MessagingException e1) {
					getBodyPartMessagingException(e1);
					return null;
				}
				String disPosition = null;
				try {
					disPosition = mbp.getDisposition();
				} catch (MessagingException e1) {
					getDispositionMessagingException(e1);
					return null;
				}
				if (disPosition == null
						|| !disPosition.equalsIgnoreCase(Part.ATTACHMENT)) {
						String more = getHtmlBody(mbp);
						if (more == null) {
							return null;
						}
						s += more;
					
				}
			}
			return s;
		}
		if(obj instanceof MimeBodyPart){
			MimeBodyPart mbp = (MimeBodyPart) obj;
			boolean isPlainText = false;
			try {
				isPlainText = mbp.isMimeType(type);
			} catch (MessagingException e) {
				isMimeTypeMessagingException(e);
				return null;
			}
			Object content = null;
			try {
				content = mbp.getContent();
			} catch (IOException e1) {
				getContentIOException(e1);
				return null;
			} catch (MessagingException e1) {
				getContentMessagingException(e1);
				return null;
			}
			if (isPlainText) {
				return (String) content;
			}
			return getHtmlBody(content);
		}
		return "";
	}
	
	private void getCountMessagingException(MessagingException e){
		e.printStackTrace();
		System.out
		.println("Exception occurs in querying the number of parts.");
	}
	
	private String getPlainBody(Object obj) {
		String type = "TEXT/PLAIN";
		if (obj instanceof MimeMessage) {
			MimeMessage mmsg = (MimeMessage) obj;
			boolean isPlainText = false;
			try {
				isPlainText = mmsg.isMimeType(type);
			} catch (MessagingException e) {
				isMimeTypeMessagingException(e);
				return null;
			}
			Object content = null;
			try {
				content = mmsg.getContent();
			} catch (IOException e1) {
				getContentIOException(e1);
				return null;
			} catch (MessagingException e1) {
				getContentMessagingException(e1);
				return null;
			}
			if (isPlainText) {
				return (String) content;
			}
			return getPlainBody(content);
			
		}
		if (obj instanceof MimeMultipart) {
			MimeMultipart mmp = (MimeMultipart) obj;
			int count = 0;
			try {
				count = mmp.getCount();
			} catch (MessagingException e) {
				getCountMessagingException(e);
				return null;
			}
			String s = "";
			for (int i = 0; i < count; ++i) {
				MimeBodyPart mbp = null;
				try {
					mbp = (MimeBodyPart) mmp.getBodyPart(i);
				} catch (MessagingException e1) {
					getBodyPartMessagingException(e1);
					return null;
				}
				String disPosition = null;
				try {
					disPosition = mbp.getDisposition();
				} catch (MessagingException e1) {
					getDispositionMessagingException(e1);
					return null;
				}
				if (disPosition == null
						|| !disPosition.equalsIgnoreCase(Part.ATTACHMENT)) {
						String more = getPlainBody(mbp);
						if (more == null) {
							return null;
						}
						s += more;
					
				}
			}
			return s;
		}
		if(obj instanceof MimeBodyPart){
			MimeBodyPart mbp = (MimeBodyPart) obj;
			boolean isPlainText = false;
			try {
				isPlainText = mbp.isMimeType(type);
			} catch (MessagingException e) {
				isMimeTypeMessagingException(e);
				return null;
			}
			Object content = null;
			try {
				content = mbp.getContent();
			} catch (IOException e1) {
				getContentIOException(e1);
				return null;
			} catch (MessagingException e1) {
				getContentMessagingException(e1);
				return null;
			}
			if (isPlainText) {
				return (String) content;
			}
			return getPlainBody(content);
		}
		return "";
	}
	
	private void getBodyPartMessagingException(MessagingException e){
		e.printStackTrace();
		System.out
		.println("Exception occurs in querying body part.");
	}
	
	private void getDispositionMessagingException(MessagingException e){
		e.printStackTrace();
		System.out
		.println("Exception occurs in querying disposition.");
	}
	
	public boolean setSubject(String subject) {
		try {
			msg.setSubject(subject);
		} catch (MessagingException e) {
			System.out.println("Cannot set email subject");
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
	public boolean setSubject(String subject,String charset) {
		try {
			msg.setSubject(subject,charset);
		} catch (MessagingException e) {
			System.out.println("Cannot set email subject");
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
	public boolean addRecipient(Message.RecipientType type, String to)  {
		try {
			msg.addRecipient(type, stringToAddress(to));
		} catch (MessagingException e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
    public boolean addTo(String to){
    	return addRecipient(Message.RecipientType.TO,to);
    }
	
    public boolean addCc(String cc){
    	return addRecipient(Message.RecipientType.CC,cc);
    }
    
    public boolean addBcc(String bcc){
    	return addRecipient(Message.RecipientType.BCC,bcc);
    }
    
    public boolean setRecipient(Message.RecipientType type, String to)  {
		try {
			msg.setRecipient(type, stringToAddress(to));
		} catch (MessagingException e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
	public boolean setTo(String to){
		return setRecipient(Message.RecipientType.TO,to);
	}
	
	public boolean setCc(String cc){
		return setRecipient(Message.RecipientType.CC,cc);
	}
	
	public boolean setBcc(String bcc){
		return setRecipient(Message.RecipientType.BCC,bcc);
	}
	
	public boolean setRecipient(Message.RecipientType type, String[] to) {
		try {
			msg.setRecipients(type, stringsToAddresses(to));
		} catch (MessagingException e) {
			System.out.println("Setting recipients failed.");
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
	public boolean setTo(String[] to){
		return setRecipient(Message.RecipientType.TO,to);
	}
	
	public boolean setCc(String[] cc){
		return setRecipient(Message.RecipientType.CC,cc);
	}
	
	public boolean setBcc(String[] bcc){
		return setRecipient(Message.RecipientType.BCC,bcc);
	}
	
	public void addFrom(String from) throws MessagingException {
		msg.addFrom(stringToAddresses(from));
	}

	public void addFrom(String[] from) throws MessagingException {
		msg.addFrom(stringsToAddresses(from));
	}

	
	public boolean setFrom(String from) {
		try {
			msg.setFrom(stringToAddress(from));
		} catch (MessagingException e) {
			System.out.println("Setting from failed.");
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
	public boolean addRecipient(Message.RecipientType type, String[] to)  {
		try {
			msg.addRecipients(type, stringsToAddresses(to));
		} catch (MessagingException e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
	public boolean addTo(String[] to){
    	return addRecipient(Message.RecipientType.TO,to);
    }
    
    public boolean addCc(String[] cc){
    	return addRecipient(Message.RecipientType.CC,cc);
    }
    
    public boolean addBcc(String[] bcc){
    	return addRecipient(Message.RecipientType.BCC,bcc);
    }
    
    private void setTextMessagingException(MessagingException e){
    	e.printStackTrace();
    	System.out.println("Unable to set text of the Message/BodyPart.");
    }
    
	public boolean setText(String text) {
		try {
			msg.setText(text);
		} catch (MessagingException e) {
			setTextMessagingException(e);
			return false;
		}
		return true;
	}
	
	public boolean setText(String text, String charset){
		try {
			msg.setText(text,charset);
		} catch (MessagingException e) {
			System.out.println("Cannot set plain text for email.");
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
	public boolean setContent(Multipart mp){
		try {
			msg.setContent(mp);
		} catch (MessagingException e) {
			setContentMessagingException(e);
			return false;
		}
		return true;
	}
	
	public boolean setContent(Object obj, String type){
		try {
			msg.setContent(obj, type);
		} catch (MessagingException e) {
			setContentMessagingException(e);
			return false;
		}
		return true;
	}
	
	private void addBodyPartMessagingException(MessagingException e){
		e.printStackTrace();
		System.out.println("Unable to add BodyPart.");
	}
	
	public Multipart buildMultipart(String text, String[] files){
		BodyPart messageBodyPart = new MimeBodyPart();
		try {
			messageBodyPart.setText(text);
		} catch (MessagingException e) {
			setTextMessagingException(e);
			return null;
		}
		Multipart multipart = new MimeMultipart();
		try {
			multipart.addBodyPart(messageBodyPart);
		} catch (MessagingException e) {
			addBodyPartMessagingException(e);
			return null;
		}
		for(int i=0; i<files.length; ++i){
			messageBodyPart = new MimeBodyPart();
			DataSource source = new FileDataSource(files[i]);
			try {
				messageBodyPart.setDataHandler(new DataHandler(source));
			} catch (MessagingException e) {
				setDataHandlerMessagingException(e);
				return null;
			}
			try {
				messageBodyPart.setFileName(files[i]);
			} catch (MessagingException e) {
				setFileNameMessagingException(e);
				return null;
			}
			try {
				multipart.addBodyPart(messageBodyPart);
			} catch (MessagingException e) {
				addBodyPartMessagingException(e);
			}
		}
		return multipart;
	}
	
	public Multipart buildMultipart(String text,String file){
		String[] files = new String[1];
		files[0] = file;
		return buildMultipart(text, files);
	}
	
	private void setFileNameMessagingException(MessagingException e){
		e.printStackTrace();
		System.out.println("Unable to set file name of the BodyPart.");
	}
	
	private void setDataHandlerMessagingException(MessagingException e){
		e.printStackTrace();
		System.out.println("Unable to set DataHandler of the BodyPart.");
	}
	
	protected Address[] stringToAddresses(String str) {
		Address[] address = new Address[1];
		try {
			address[0] = new InternetAddress(str);
		} catch (AddressException e) {
			System.out.println(str + " is not a valid email address");
		}
		return address;
	}

	public static String[] addressesToStrings(Address[] ads) {
		String[] str = new String[ads.length];
		for (int i = ads.length - 1; i >= 0; --i) {
			str[i] = ((InternetAddress) ads[i]).getAddress();
		}
		return str;

	}
	
	public static String addressToString(Address ad) {
		return ((InternetAddress) ad).getAddress();
	}

	protected Address[] stringsToAddresses(String[] str) {
		Address[] address = new Address[str.length];
		for (int i = 0; i < address.length; ++i) {
			try {
				address[i] = new InternetAddress(str[i]);
			} catch (AddressException e) {
				System.out.println(str[i] + " is not a valid email address.");
			}
		}
		return address;
	}

	protected Address stringToAddress(String str) {
		Address address;
		try {
			address = new InternetAddress(str);
			return address;
		} catch (AddressException e) {
			System.err.println(str + " is not a valid email address.");
		}
		return null;
	}
	
	
}
