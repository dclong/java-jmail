//The JavaMailSender object is reusable. 
//You only need to authenticate once.
package dclong.net;

public class TestJavaMailSender {
	public static void main(String[] args)
    {
		JavaMailSender jmail = new JavaMailSender("smtp.gmail.com","ssl",465);
		jmail.setTo("duchuanlong@gmail.com");
		jmail.setSubject("ssl authentication");
		jmail.setText("please ignore it");
		//jmail.setContent(jmail.buildMultipart("email with attachments","/home/dclong/operator.r"));
//		jmail.authenticate();
		jmail.authenticate("firedragon.du@gmail.com", "!2345678");
		System.out.println(jmail.send());
    }
}
