package dclong.net;

import java.util.Properties;
import javax.mail.Flags;
import javax.mail.Folder;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.NoSuchProviderException;
import javax.mail.Session;
import javax.mail.Store;
import javax.mail.Flags.Flag;
import javax.mail.internet.MimeMessage;
import javax.mail.search.FlagTerm;

public class JavaMailReceiver extends JavaMailer{
	
	private String protocol;
			
	private Store store;
	
	private Folder folder;
	
	private Message messages[];
		
	public JavaMailReceiver(String protocol){
		props = new Properties();
		session = Session.getInstance(props, null);
		this.protocol = protocol;
		try {
			store = session.getStore(protocol);
		} catch (NoSuchProviderException e) {
			System.out.println("The specified protocal is not supported.");
			e.printStackTrace();
		}
	}
	
	
//	public boolean createStore(String protocol){
//		try {
//			store = session.getStore(protocol);
//		} catch (NoSuchProviderException e) {
//			System.out.println("The specified protocal is not supported.");
//			e.printStackTrace();
//			return false;
//		}
//		return true;
//	}
	
	public boolean authenticate(String host,String userName,String password){
		try {
			store.connect(host, userName, password);
		} catch (MessagingException e) {
			System.out.println("A connection to the server cannot be established.");
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
//	public void destroy(){
//		closeFolder(true);
//		try {
//			store.close();
//		} catch (MessagingException e) {
//			e.printStackTrace();
//			System.out.println("The store has ");
//		}
//	}
	
	public void closeFolder(boolean expunge){
		try {
			if(folder.isOpen()){
				folder.close(expunge);
			}
		} catch (MessagingException e) {
			e.printStackTrace();
			System.out.println("The folder has already been close. This is probably due to loss of connection.");
		}
	}
	/**
	 * 
	 * @param folder
	 * @param access 1 -- Read only.
	 * 2 -- Read and write.
	 * @return
	 */
	public boolean openFolder(String folder,int access){
		try {
			this.folder = store.getFolder(folder);
		} catch (MessagingException e) {
			System.out.println("The folder type is not supported.");
			e.printStackTrace();
			return false;
		}
		try {
			this.folder.open(access);
		} catch (MessagingException e) {
			System.out.println("Cannot open the folder.\n");
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
	public boolean openFolder(String folder){
		return openFolder(folder,Folder.READ_ONLY);
	}

	
	public int getUnreadEmailsCount(){
		try {
			return folder.getUnreadMessageCount();
		} catch (MessagingException e) {
			System.out.println("Counting number of messages is not support by " + protocol + " protocol.");
			e.printStackTrace();
			return -1;
		}
	}

	public int getDeletedEmailsCount(){
		try {
			return folder.getDeletedMessageCount();
		} catch (MessagingException e) {
			System.out.println("Counting number of messages is not support by " + protocol + " protocol.");
			e.printStackTrace();
			return -1;
		}
	}
	
	public int getNewEmailsCount(){
		try {
			return folder.getNewMessageCount();
		} catch (MessagingException e) {
			e.printStackTrace();
			System.out.println("Counting number of messages is not support by " + protocol + " protocol.");
			return -1;
		}
	}
	
	public int getBufferedEmailsCount(){
		return messages.length;
	}
	
	public Message getBufferedEmail(int index){
		if(index<messages.length){
			return messages[index];
		}
		return null;
	}
	
	public void selectBufferedEmail(int index){
		if(index<messages.length){
			msg = (MimeMessage)messages[index];
			return;
		}
		msg = null;
	}
	
	public boolean searchUnreadEmails(boolean current){
		if(current){
			try {
				messages = folder.search(new FlagTerm(new Flags(Flag.SEEN), false),messages);
			} catch (MessagingException e) {
				System.out.println("The SEEN flag is not supported by "+protocol+" protocol.");
				e.printStackTrace();
				return false;
			}
			return true;
		}
		try {
			messages = folder.search(new FlagTerm(new Flags(Flag.SEEN), false));
		} catch (MessagingException e) {
			System.out.println("The SEEN flag is not supported by "+protocol+" protocol.");
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
	public boolean searchReadEmails(boolean current){
		if(current){
			try {
				messages = folder.search(new FlagTerm(new Flags(Flag.SEEN), false),messages);
			} catch (MessagingException e) {
				System.out.println("The SEEN flag is not supported by "+protocol+" protocol.");
				e.printStackTrace();
				return false;
			}
			return true;
		}
		try {
			messages = folder.search(new FlagTerm(new Flags(Flag.SEEN), true));
		} catch (MessagingException e) {
			System.out.println("The SEEN flag is not supported by "+protocol+" protocol.");
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
	public boolean searchUnRepliedEmails(boolean current){
		if(current){
			try {
				messages = folder.search(new FlagTerm(new Flags(Flag.SEEN), false),messages);
			} catch (MessagingException e) {
				System.out.println("The SEEN flag is not supported by "+protocol+" protocol.");
				e.printStackTrace();
				return false;
			}
			return true;
		}
		try {
			messages = folder.search(new FlagTerm(new Flags(Flag.ANSWERED), false));
		} catch (MessagingException e) {
			System.out.println("The SEEN flag is not supported by "+protocol+" protocol.");
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
	public boolean searchRepliedEmails(boolean current){
		if(current){
			try {
				messages = folder.search(new FlagTerm(new Flags(Flag.SEEN), false),messages);
			} catch (MessagingException e) {
				System.out.println("The SEEN flag is not supported by "+protocol+" protocol.");
				e.printStackTrace();
				return false;
			}
			return true;
		}
		try {
			messages = folder.search(new FlagTerm(new Flags(Flag.ANSWERED), true));
		} catch (MessagingException e) {
			System.out.println("The SEEN flag is not supported by "+protocol+" protocol.");
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
	public boolean searchDeletedEmails(boolean current){
		if(current){
			try {
				messages = folder.search(new FlagTerm(new Flags(Flag.SEEN), false),messages);
			} catch (MessagingException e) {
				System.out.println("The SEEN flag is not supported by "+protocol+" protocol.");
				e.printStackTrace();
				return false;
			}
			return true;
		}
		try {
			messages = folder.search(new FlagTerm(new Flags(Flag.DELETED), true));
		} catch (MessagingException e) {
			System.out.println("The SEEN flag is not supported by "+protocol+" protocol.");
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
	public boolean searchDraftEmails(boolean current){
		if(current){
			try {
				messages = folder.search(new FlagTerm(new Flags(Flag.SEEN), false),messages);
			} catch (MessagingException e) {
				System.out.println("The SEEN flag is not supported by "+protocol+" protocol.");
				e.printStackTrace();
				return false;
			}
			return true;
		}
		try {
			messages = folder.search(new FlagTerm(new Flags(Flag.DRAFT), true));
		} catch (MessagingException e) {
			System.out.println("The SEEN flag is not supported by "+protocol+" protocol.");
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
	public boolean deleteSelectedEmail(boolean delete){
		return setFlag(Flag.DELETED,delete);
	}
	
	public boolean deleteBufferedEmails(boolean delete){
		return setFlags(new Flags(Flags.Flag.DELETED),delete);
	}
	
	public boolean setFlags(Flags flag, boolean value){
		try {
			folder.setFlags(messages, flag, value);
			return true;
		} catch (MessagingException e) {
			setFlagsMessagingException(e,flag);
			return false;
		}
	}
	
	private void setFlagsMessagingException(MessagingException e, Flags flag){
		e.printStackTrace();
		System.out.println("Unable to set the \"" + flag + "\" flag of buffered emails");
	}
	
	public boolean setFlag(Flag flag,boolean value) {
		try {
			msg.setFlag(flag, value);
			return true;
		} catch (MessagingException e) {
			setFlagMessagingException(e,flag);
			return false;
		}
	}
	
	private void setFlagMessagingException(MessagingException e,Flag flag){
		e.printStackTrace();
		System.out.println("Unable to set \"" + flag + "\" flag of the message.");
	}
	
	public boolean searchFlaggedEmails(boolean current){
		if(current){
			try {
				messages = folder.search(new FlagTerm(new Flags(Flag.SEEN), false),messages);
			} catch (MessagingException e) {
				System.out.println("The SEEN flag is not supported by "+protocol+" protocol.");
				e.printStackTrace();
				return false;
			}
			return true;
		}
		try {
			messages = folder.search(new FlagTerm(new Flags(Flag.FLAGGED), true));
		} catch (MessagingException e) {
			System.out.println("The SEEN flag is not supported by "+protocol+" protocol.");
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
	public boolean searchUnflaggedEmails(boolean current){
		if(current){
			try {
				messages = folder.search(new FlagTerm(new Flags(Flag.SEEN), false),messages);
			} catch (MessagingException e) {
				System.out.println("The SEEN flag is not supported by "+protocol+" protocol.");
				e.printStackTrace();
				return false;
			}
			return true;
		}
		try {
			messages = folder.search(new FlagTerm(new Flags(Flag.FLAGGED), false));
		} catch (MessagingException e) {
			System.out.println("The SEEN flag is not supported by "+protocol+" protocol.");
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
	public boolean fetchAllEmails(){
			try {
				messages = folder.getMessages();
			} catch (MessagingException e) {
				System.out.println("Fetching all emails failed!\n");
				e.printStackTrace();
				return false;
			}
			return true;
	}
	
	public boolean fetchEmails(int[] emailNumbers){//1-based
		try {
			messages = folder.getMessages(emailNumbers);
		} catch (MessagingException e) {
			System.out.println("Fetching emails failed!\n");
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
	public boolean fetchEmails(int startNumber,int endNumber){//1-based, inclusive-exclusive
		try {
			messages = folder.getMessages(startNumber, endNumber);
		} catch (MessagingException e) {
			System.out.println("Fetching emails failed!");
			e.printStackTrace();
			return false;
		}
		return true;
	}
}
