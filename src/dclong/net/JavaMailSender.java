package dclong.net;

import java.util.Properties;
import javax.mail.MessagingException;
import javax.mail.NoSuchProviderException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.MimeMessage;

public class JavaMailSender extends JavaMailer{
	// ------------------------------------------------------------
	private Transport transport;
	// --------------------------------------------------------------------
	/**
	 * 
	 * @param host
	 * @param encryption encryption type. 
	 * Supported types are "tls", "starttls" and "ssl".
	 * You must set an appropriate port if you use encryption. 
	 */
	public JavaMailSender(String host, String encryption) {
		// initialize some important instance variables first
		props = new Properties();
		if(host!=null){
			setHost(host);
		}
		if(encryption != null){
			setEncryption(encryption);
		}
		session = Session.getInstance(props, null);
		msg = new MimeMessage(session);
		try {
			transport = session.getTransport("smtp");
		} catch (NoSuchProviderException e) {
			System.out
					.println("A transport implement SMTP protocol cannot be created.");
			e.printStackTrace();
		}
	}
	
	public void setHost(String host) {
		props.setProperty("mail.smtp.host", host);
	}
	public void setEncryption(String encryption){
		encryption = encryption.toLowerCase();
		props.setProperty("mail.smtp." + encryption + ".enable", String.valueOf(true));
	}
	
	public JavaMailSender(){
		this(null,null);
	}

	public JavaMailSender(String host){
		this(host,null);
	}
	
	public JavaMailSender(String host, String encryption, int port) {
		this(host,encryption);
		setPort(port);
	}

	public JavaMailSender(String host,int port){
		this(host,null,port);
	}
	/**
	 * Authenticate connection with the host. If the SMTP server support sending
	 * mail without authentication, you can use any string for the user name and
	 * password.
	 * 
	 * @param username
	 *            user name on the SMTP server.
	 * @param password
	 *            password for the user name.
	 * @throws MessagingException
	 */
	public boolean authenticate(String host, String username, String password) {
		return close() & connect(host, username, password);
	}

	public boolean authenticate(String host, int port, String username,
			String password) {
		return close() & connect(host, port, username, password);
	}

	public boolean authenticate(String username, String password) {
		return close() & connect(username, password);
	}
	
	public boolean authenticate(String username){
		return authenticate(username,"");
	}

	public boolean authenticate() {
		return close() & connect();
	}

	

	public void setPort(int port) {
		props.setProperty("mail.smtp.port", String.valueOf(port));
		props.setProperty("mail.smtp.socketFactory.port", String.valueOf(port));
	}

	public void setDebug(boolean debug) {
		props.setProperty("mail.smtp.debug", String.valueOf(debug));
		session.setDebug(debug);
	}

	public void setSocketFactoryClass(String socketFactoryClass) {
		props.setProperty("mail.smtp.socketFactory.class", socketFactoryClass);
	}

	public void setFallback(boolean fallback) {
		props.setProperty("mail.smtp.socketFactory.fallback",
				String.valueOf(fallback));
	}
	
	public boolean send() {
		try {
			transport.sendMessage(msg, msg.getAllRecipients());
		} catch (MessagingException e) {
			System.out.println("Message sending failed.\n");
			e.printStackTrace();
			return false;
		}
		return true;
	}

	private boolean connect() {
		if (!transport.isConnected()) {
			try {
				transport.connect();
			} catch (MessagingException e) {
				System.out
						.println("A connection to the server cannot be established.");
				e.printStackTrace();
				return false;
			}
		}
		return true;
	}

	private boolean connect(String userName, String password) {
		if (!transport.isConnected()) {
			try {
				transport.connect(userName, password);
			} catch (MessagingException e) {
				System.out
						.println("A connection to the server cannot be established.");
				e.printStackTrace();
				return false;
			}
		}
		return true;
	}

	private boolean connect(String host, String userName, String password) {
		if (!transport.isConnected()) {
			try {
				transport.connect(host, userName, password);
			} catch (MessagingException e) {
				System.out
						.println("A connection to the server cannot be established.");
				e.printStackTrace();
				return false;
			}
		}
		return true;
	}

	private boolean connect(String host, int port, String userName,
			String password) {
		if (!transport.isConnected()) {
			try {
				transport.connect(host, port, userName, password);
			} catch (MessagingException e) {
				System.out
						.println("A connection to the server cannot be established.");
				e.printStackTrace();
				return false;
			}
		}
		return true;
	}

	public boolean isConnected(){
		return transport.isConnected();
	}
	
	public boolean close() {
		if (isConnected()) {
			try {
				transport.close();
			} catch (MessagingException e) {
				System.out
						.println("The current connection to the server cannot be terminated.");
				e.printStackTrace();
				return false;
			}
		}
		return true;
	}

}